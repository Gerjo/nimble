#include "Worker.h"
#include "Nimble.h"
#include "Client.h"

Worker::Worker(Nimble& nimble) : 
    _nimble(nimble),
    _settingsManager(nimble.getSettingsManager()) {

}

Worker::~Worker() {

}

const int Worker::readFromClient(Client * client) {
    const int readChunkSize = 4048; // TODO: determine some good avarage?
    int bytesRead = 0;
    int timeout   = 0;

    do {
        int memoryAvailable = client->allocated - client->headerLength;

        assert(memoryAvailable >= 0);

        if(memoryAvailable == 0) {
            client->allocated += readChunkSize;

            client->c_headers  = (char*)realloc(client->c_headers, sizeof(char*) * client->allocated);

            // Doesn't seem to be 100% thread safe, either this call is not safe, or this call
            // is called simultaneously (race condition). Must thoroughly test this with a mutex.
            memset(client->c_headers + (client->allocated - readChunkSize), 0, readChunkSize);

            // So, what do we fall back to here?
            if(client->c_headers == 0) {
                perror("realloc failed.");
                exit(21);
            }

            memoryAvailable = readChunkSize;

            //cout /**/ << "[" << client->fd << "] [" << pthread_self() << "] Allocated " << readChunkSize << " bytes. Total: " << client->allocated << " bytes" << endl;
        }

        //cout /**/ << "[" << client->fd << "] [" << pthread_self() << "] Reading at offset " << client->headerLength << " for up to " << memoryAvailable << " bytes." << endl;
        bytesRead = ::recv(client->fd, client->c_headers + client->headerLength, memoryAvailable, MSG_NOSIGNAL);
        //cout /**/ << "[" << client->fd << "] [" << pthread_self() << "] Finished reading of " << bytesRead << " bytes." << endl;

        if(bytesRead == -1) {
            if(errno == EAGAIN) { // 11
                //cout /**/ << "[" << client->fd << "] [" << pthread_self() << "] end of data! EAGAIN" << endl;
                break;
            }

            perror("Cannot receive from client");

            //cout /**/ << "[" << client->fd << "] [" << pthread_self() << "] Reading error. Errno: " << errno << endl;
            //break;
        }

        // Data isn't quite ready yet, try again - it will be ready. // HTTP/1.1 issue ?
        //if(bytesRead == -1 && errno == EAGAIN) {
        //    cout << "try again" << endl;
        //    continue;
        //}

        client->headerLength += bytesRead;

        // We read less than we wanted, so assume there is no more data.
        if(bytesRead < memoryAvailable) {
            //cout /**/ << "[" << client->fd << "] [" << pthread_self() << "] no more data: " << bytesRead << "/" << memoryAvailable << endl;
            break;
        }

    } while (timeout++ < 1000);

    if(timeout > 999) {
        cout << " Cannot read from client, the do...while(1) had a time out. " << endl;
        exit(999);
    }

    //cout /**/ << "[" << client->fd << "] [" << pthread_self() << "] Done with reading. Scanning for EOF" << endl;

    // At somepoint we'll have timeouts:
    client->timeLastRead = time(0);

    return bytesRead;
}

void Worker::sendResponse(Client* client, const char* start, const int& totalLength) {

    int total         = 0;
    int bytesSent     = 0;

    do {
        bytesSent = ::send(client->fd, (void*) (start + total), totalLength, MSG_NOSIGNAL);

        if (bytesSent == -1) {
            if (errno == EINTR || errno == EAGAIN) {
                continue;
            }
            
            perror("Cannot send to client because");
            break;
        }

        if ((total += bytesSent) < totalLength) {
            continue;
        }

        // We reached this point, so all data is sent.
        break;
    } while (true);

    _nimble.closeConnection(client);
}

bool Worker::isEndOfRequest(Client* client) {

    const int& strlen = client->headerLength;

    // "\r\n\r\n" 13 10 13 10 (RFC compliant browsers?)
    if (client->headerLength > 4 &&
            *(client->c_headers + (strlen - 1)) == 10 &&
            *(client->c_headers + (strlen - 2)) == 13 &&
            *(client->c_headers + (strlen - 3)) == 10 &&
            *(client->c_headers + (strlen - 4)) == 13) {

        return true;

    // "\r\r" 10 10 (netcat and telnet)
    } else if (client->headerLength > 2 &&
            *(client->c_headers + (strlen - 1)) == 10 &&
            *(client->c_headers + (strlen - 2)) == 10) {

        return true;
    }

    return false;
}



void Worker::run(void) {
    while (1) {
        Client* client = _nimble._stack.pop(); // TODO: fix sleepPop
        //cout << "mehmeh" << endl;

        if (client != 0) {

           
            //printf("mehmeh \n");

            const int bytesRead = readFromClient(client);

            if(bytesRead > 0) {
                if (isEndOfRequest(client)) {

                    HttpHeaders headers = HeaderParser::parse(client->c_headers, client->headerLength);
                    Host* host          = _settingsManager.getHostFor(headers);

                    stringstream filePath;
                    filePath << host->getRoot();
                    filePath.write(headers.file.string, headers.file.length);
                    // TODO: file sanity check, see if it's in the public html root folder.


                    if(host != 0) {
                        stringstream response;
                        response << "HTTP/1.0 200 OK\r\n";
                        response << "Content-Type: text/html\r\n\r\n";

                        if(Helper::FileExists(filePath.str().c_str())) {
                            string data = Helper::GetFileContents(filePath.str().c_str());
                            response << data;
                            response << "File: " << filePath.str();
                        } else {
                            response << "<h1>Nimble received your request.</h1><hr><b>Here are some parsing details:</b><br>";
                            response << "<br>Request method: (" << headers.method.length << " bytes) " << string(headers.method.string, headers.method.length);
                            response << "<br>File name: (" << headers.file.length << " bytes) " << string(headers.file.string, headers.file.length);
                            response << "<br>File extension: (" << headers.extension.length << " bytes) " << string(headers.extension.string, headers.extension.length);
                            response << "<br>Query string: (" << headers.queryString.length << " bytes) " << string(headers.queryString.string, headers.queryString.length);
                            response << "<br>Hostname: (" << headers.host.length << " bytes) " << string(headers.host.string, headers.host.length);
                            response << "<br>Port: (" << headers.port.length << " bytes) " << string(headers.port.string, headers.port.length);
                            response << "<hr><b>All received headers: </b><pre>";
                            response << string(client->c_headers, client->headerLength);
                            response << "File path: '" << filePath.str() << "'";
                            response << "</pre>";
                        }
                       

                        response << "\r\n";
                        sendResponse(client, response.str().c_str(), response.str().length());
                    }
                }
            }

        } else {
            usleep(5000); // ZZzzZzzzz
        }
    }
}
