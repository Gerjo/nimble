#ifndef REQUESTSTACK_H
#define	REQUESTSTACK_H

#include <pthread.h>
#include <vector>
#include <stdlib.h>
#include "Client.h"
#include "Semaphore.h"

using namespace std;

// NB.: All code is contained within this header, I haven't got the slightest
// clue how to span templates from a header file into a .cpp file. Help
// is welcome *sigh* -- Gerjo

// Future TODO: It seems that I'm grabbing a stack rather quickly when a
// semaphore might be more efficient and avoids any busy-waiting situations -- Gerjo

template <class T>
class ThreadStack {
    public:
        ThreadStack() : _semaphore(0) {
            // TODO: this isn't very gracefull...
            _clients  = (T*)malloc(sizeof(T) * 10000);
            _size     = 0;
            pthread_mutex_init(&_popMutex, NULL);
        }

        void push(T client) {
            _semaphore.signal();

            // Perhaps this needs a mutex, too?
            this->_clients[_size++] = client;
        }

        T pop() {
            T returnClient = 0;

            pthread_mutex_lock(&_popMutex);

            if(_size > 0) {
                returnClient = this->_clients[--_size];
            }

            pthread_mutex_unlock(&_popMutex);

            return returnClient;
        }

        T sleepPop() {
            _semaphore.wait(); // ZZzzzzz
            
            return pop();//this->_clients[--_size];
        }
    private:
        Semaphore _semaphore;
        int _size;
        pthread_mutex_t _popMutex;
        T* _clients;
};

#endif	/* REQUESTSTACK_H */

