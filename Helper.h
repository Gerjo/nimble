#ifndef HELPER_H
#define	HELPER_H

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class Helper {
    public:
        static string GetFileContents(const char* fileName) {
            ifstream file (fileName, ios::in|ios::binary|ios::ate);
            const int size = file.tellg();
            char memblock[size];

            file.seekg(0, ios::beg);
            file.read(memblock, size);
            file.close();

            std::string fileString(memblock, size);
            
            cout << " Read " << size << " bytes from: " << fileName << endl;

            return fileString;
        }

        static bool FileExists(const char* fileName) {
            ifstream ifile(fileName);
            return ifile ? true : false;
        }
};

#endif	/* HELPER_H */

