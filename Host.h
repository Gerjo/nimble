#ifndef HOST_H
#define	HOST_H

#include "elements.h"


#include "reader.h"

using namespace std;

class Host {
    public:
        Host(const json::Object& asJson) {
            _port = (json::Number) asJson["port"];
            _root = (json::String) asJson["root"];
            _host = (json::String) asJson["host"];
        }

        ~Host() {
            
        }

        void setRoot(string _root) {
            this->_root = _root;
        }

        const string& getRoot() const {
            return _root;
        }

        void setHost(string _hostname) {
            this->_host = _hostname;
        }

        const string& getHost() const {
            return _host;
        }

        void setPort(int _port) {
            this->_port = _port;
        }

        const int& getPort() const {
            return _port;
        }

    private:
        Host(const Host& orig) { }

        int _port;
        string _host;
        string _root;
};

#endif	/* HOST_H */

