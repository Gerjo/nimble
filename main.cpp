#include <cstdlib>
#include <iostream>
#include <sys/types.h>
#include "Nimble.h"
#include "TestSuite.h"
#include "HeaderParser.h"

using namespace std;

int main(int argc, char** argv) {
    
    /*pid_t pid = fork();
    if (pid == 0) {
        while(1) {
            sleep(1);
            cout << "child alive" << endl;
        }
    } else if (pid < 0) {
        cout << "fork error" << endl;
    } else {
        while(1) {
            sleep(1);
            cout << "master alive" << endl;
        }
    } */

    TestSuite testSuite;
    testSuite.run();

    Nimble nimble;

    return 0;
}

